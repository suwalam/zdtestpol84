package pl.sda.objects;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookServiceTest {

    private BookService underTest;

    @BeforeEach
    public void setUp() {
        underTest = new BookService(5);
        underTest.add(new Book("Adam Mickiewicz", "Pan Tadeusz", 1122,
                1873, 5, "opis"));

        underTest.add(new Book("Henryk Sienkiewicz", "Potop", 1522,
                1883, 5, "opis"));
    }

    @Test
    public void shouldAddBook() {
        //given
        Book testBook = new Book("Henryk Sienkiewicz", "Ogniem i Mieczem", 1322,
                1893, 8, "opis");

        //when
        underTest.add(testBook);

        //then
        Book result = underTest.get(2);
        Assertions.assertNotNull(result);
        Assertions.assertEquals(testBook.getYear(), result.getYear());
        Assertions.assertEquals(testBook.getAuthor(), result.getAuthor());
        Assertions.assertEquals(3, underTest.size());
    }

    @Test
    public void shouldRemoveBook() {
        //given
        int index = 1;

        //when
        Book removed = underTest.remove(index);

        //then
        Assertions.assertNotNull(removed);
        Assertions.assertEquals(1, underTest.size());
        Assertions.assertEquals("Henryk Sienkiewicz", removed.getAuthor());
    }


}