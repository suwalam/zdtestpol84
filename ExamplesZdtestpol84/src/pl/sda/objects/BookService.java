package pl.sda.objects;

public class BookService {

    private Book[] books;

    private int size;

    private int counter;

    public BookService(int size) {
        this.size = size;
        this.books = new Book[size];
    }

    public void add(Book book) {

        if (counter == size) {
            System.out.println("Zbiór danych jest pełny. Nie można dodać kolejnej książki!");
            return;
        }

        books[counter] = book;
        counter++;
    }

    public Book get(int index) {

        if (isValidIndex(index)) {
            return books[index];
        }

        return null;

    }

    public Book remove(int index) {

        Book removed = null;

        if (isValidIndex(index)) {
            removed = books[index];

            for (int i = index; i <= counter-1; i++) {
               books[i] = books[i+1];
            }

            counter--;
        }

        return removed;
    }

    private boolean isValidIndex(int index) {
        if (index < 0 || index > counter) {
            System.out.println("Indeks wykracza poza zakres zbioru!");
            return false;
        }

        return true;
    }

    public int size() {
        return counter;
    }

}
