package pl.sda.objects;

//Klasa POJO
public class Book {

    private String author;
    private String title;
    private int pages;
    private int year;
    private int month;
    private String description;

    public Book(String author, String title, int pages, int year, int month, String description) {
        this.author = author;
        this.title = title;
        this.pages = pages;
        this.year = year;
        this.month = month;
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", pages=" + pages +
                ", year=" + year +
                ", month=" + month +
                ", description='" + description + '\'' +
                '}';
    }

   public boolean containsDescription(String search) {
        return description.contains(search);
   }

   public void printReleased(int releaseYear) {
        if (year < releaseYear) {
            System.out.println("Książka wydana przed rokiem " + releaseYear);
        } else if (year > releaseYear) {
            System.out.println("Książka wydana po roku " + releaseYear);
        } else {
            System.out.println("Książka wydana w roku " + releaseYear);
        }
   }

}
