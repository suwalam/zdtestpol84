package pl.sda.objects;

public class BookMain {

    public static void main(String[] args) {

        Book book1 = new Book("Adam Mickiewicz", "Pan Tadeusz", 1122,
                1873, 5, "opis");

        book1.setDescription("aktualizacja opisu");

        //sout
        System.out.println(book1);

        System.out.println(book1.containsDescription("opis"));

        book1.printReleased(1800);

    }


}
